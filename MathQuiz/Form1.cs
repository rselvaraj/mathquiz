﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace MathQuiz
{
    public partial class Form1 : Form
    {
        // Create a Random object to generate random numbers.
        Random randomizer = new Random();

        // These ints will store the numbers 
        // for the addition problem.
        int addend1;
        int addend2;

        // These ints will store the numbers 
        // for the subtraction problem. 
        int minuend;
        int subtrahend;

        // These ints will store the numbers for the multiplication problem. 
        int multiplicand;
        int multiplier;

        // These ints will store the numbers for the division problem. 
        int dividend;
        int divisor;

        // This int will keep track of the time left. 
        int timeLeft;

         // Create an instance of the SoundPlayer class.
        SoundPlayer correctSound = new SoundPlayer(@"c:\Windows\Media\chimes.wav");
        SoundPlayer WrongSound = new SoundPlayer(@"c:\Windows\Media\chord.wav");

        public Form1()
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Start the quiz by filling in all of the problems 
        /// and starting the timer. 
        /// </summary> 
        public void StartTheQuiz()
        {
            // Fill in the addition problem.
            addend1 = randomizer.Next(51);
            addend2 = randomizer.Next(51);

            plustLeftLabel.Text = addend1.ToString();
            plusRightLabel.Text = addend2.ToString();

            sum.Value = 0;

            // Fill in the subtraction problem.
            minuend = randomizer.Next(1, 101);
            subtrahend = randomizer.Next(1, minuend);
            minusLeftLabel.Text = minuend.ToString();
            minusRightLabel.Text = subtrahend.ToString();
            difference.Value = 0;

            // Fill in the multiplication problem.
            multiplicand = randomizer.Next(2, 11);
            multiplier = randomizer.Next(2, 11);
            timesLeftLabel.Text = multiplicand.ToString();
            timesRightLabel.Text = multiplier.ToString();
            product.Value = 0;

            // Fill in the division problem.
            divisor = randomizer.Next(2, 11);
            int temporaryQuotient = randomizer.Next(2, 11);
            dividend = divisor * temporaryQuotient;
            dividedLeftLabel.Text = dividend.ToString();
            dividedRightLabel.Text = divisor.ToString();
            quotient.Value = 0;

            // Start the timer.
            
            timeLeft = 30;
            timeLabel.Text = "30 seconds";
            timer1.Start();

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void startbutton_Click(object sender, EventArgs e)
        {
            StartTheQuiz();
            startButton.Enabled = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (CheckTheAnswer())
            {
                // If the user got the answer right, stop the timer  
                // and show a MessageBox.
                timer1.Stop();
                MessageBox.Show("You got all the answers right!",
                                "Congratulations");
                startButton.Enabled = true;
            }
            else if (timeLeft > 0)
            {
                // Display the new time left 
                // by updating the Time Left label.
                timeLeft = timeLeft - 1;
                if (timeLeft == 5) { timeLabel.BackColor = Color.Red; }
                timeLabel.Text = timeLeft + " seconds";
            }
            else
            {
                // If the user ran out of time, stop the timer, show 
                // a MessageBox, and fill in the answers.
                timer1.Stop();
                timeLabel.Text = "Time's up!";
                MessageBox.Show("You didn't finish in time.", "Sorry");
                sum.Value = addend1 + addend2;
                difference.Value = minuend - subtrahend;
                product.Value = multiplicand * multiplier;
                quotient.Value = dividend / divisor;

                timeLabel.BackColor = Color.FromName("Control");

                startButton.Enabled = true;
            }
        }

        /// <summary> 
        /// Check the answer to see if the user got everything right. 
        /// </summary> 
        /// <returns>True if the answer's correct, false otherwise.</returns> 
        private bool CheckTheAnswer()
        {
            if ((addend1 + addend2 == sum.Value)
                    && (minuend - subtrahend == difference.Value)
                    && (multiplicand * multiplier == product.Value)
                    && (dividend / divisor == quotient.Value))
                return true;
            else
                return false;
        }

        private void answer_Enter(object sender, EventArgs e)
        {
            // Select the whole answer in the NumericUpDown control.
            NumericUpDown answerBox = sender as NumericUpDown;

            if (answerBox != null)
            {
                int lengthOfAnswer = answerBox.Value.ToString().Length;
                answerBox.Select(0, lengthOfAnswer);
            }
        }
   

        private void NumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown answerBox = sender as NumericUpDown;
            Boolean AnswerIsRight = false;

            if (answerBox != null)
            {
                String controlName = answerBox.Name;

                if (controlName == "sum") {
                    if (addend1 + addend2 == sum.Value) { AnswerIsRight = true;}
                } else if (controlName == "difference")
                {
                    if (minuend - subtrahend == difference.Value) { AnswerIsRight = true; }
                }
                else if (controlName == "product")
                { 
                    if (multiplicand * multiplier == product.Value) { AnswerIsRight = true; } 
                }
                else if (controlName == "quotient") 
                { 
                    if (dividend / divisor == quotient.Value) { AnswerIsRight = true; } 
                }
            }

            if (AnswerIsRight) { correctSound.Play();}
            else { WrongSound.Play(); }
        }

    }
}
